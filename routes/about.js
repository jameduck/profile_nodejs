var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('about', { 
  title: 'Nattapat Khowphong | About' ,
  name: 'Nattapat Khowphong',
  sid: '56130010182',
  p_name: 'ชื่อ: นายณัฐภัทร ขาวผ่อง',
  tel: '083-032-2XXX',
  sex: 'ชาย',
  bod: '29 สิงหาคม 2537',
  email: 'nattapat.rbz@g.swu.ac.th'
  });
});

module.exports = router;
