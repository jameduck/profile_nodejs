var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('favourite', { 
  title: 'Nattapat Khowphong | About' ,
  name: 'Nattapat Khowphong',
  sid: '56130010182',
  p_name: 'ชื่อ: นายณัฐภัทร ขาวผ่อง',
  tel: '083-032-2XXX',
  sex: 'ชาย',
  bod: '29 สิงหาคม 2537',
  email: 'nattapat.rbz@g.swu.ac.th' ,
  study1: 'โรงเรียนปราโมชวิทยารามอินทรา' ,
  study2: 'โรงเรียนสตรีวิทยา 2' ,
  study3: 'มหาวิทยาลัยศรีนครินทรวิโรฒ',
  food1: 'Dominos Pizza',
  food2: 'Texas Chicken',
  food3: 'Ryu Shabu Shabu',
  music1: 'AKB48',
  music2: 'Silent Siren',
  music3: 'Goose house',

  });
});

module.exports = router;
