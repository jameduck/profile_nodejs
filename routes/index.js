var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { 
  title: 'Nattapat Khowphong | Profile' ,
  name: 'Nattapat Khowphong',
  sid: '56130010182'
  });
});

module.exports = router;
